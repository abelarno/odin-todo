import { render } from "./render.js";
import { nav } from "./nav.js";


export var todoList = (() => {
  var currentProject = 0;

  var todo = [
    {
      name: "Default",
      todos: [
        {
          title: "Default",
          description: "test",
          dueDate: "test",
          priority: 0,
        },
        {
          title: "test2",
          description: "test2",
          dueDate: "test2",
          priority: 0,
        },
      ],
    },

    {
      name: "Project2",
      todos: [
        {
          title: "Project2",
          description: "test",
          dueDate: "test",
          priority: 0,
        },
        {
          title: "Project2",
          description: "2",
          dueDate: "test",
          priority: 0,
        },
      ],
    },
  ];

  function show(e) {
    return todo[e];
  }

  function showAll() {
    return todo;
  }

  function add(title, description, dueDate, priority, project) {
    todo[project].todos.push({
      title: title,
      description: description,
      dueDate: dueDate,
      priority: priority,
    });
    save();
    render(project);
  }

  function remove(element, arrayId) {
    console.log(todo[arrayId].todos[element.id]);
    todo[arrayId].todos.splice(element.id, 1);
    save();
    render(arrayId);
  }

  function addProject(newName) {
    console.log("add project")
    todo.push({name: newName, todos: []})
    nav();
    save();
  }

  function removeProject(id) {
    todo.splice(id,1)
    save();
    nav()
    render(0);
  }

  function save() {
    localStorage.setItem("list", JSON.stringify(todo));
    console.log("saved");
  }

  function load() {
    const savedlist = JSON.parse(localStorage.getItem("list"));
    if (savedlist) {
      todo = savedlist;
    } else {
      alert("no data!");
    }
  }

  return {
    show: show,
    showAll: showAll,
    add: add,
    remove: remove,
    load: load,
    addProject: addProject,
    removeProject: removeProject,
    currentProject: currentProject,
  };
})();
