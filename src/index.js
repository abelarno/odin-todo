import { render } from "./render.js";
import { todoList } from "./todolist.js";
import { nav } from "./nav.js";

todoList.load();
nav();
render(0);

const savedlist = JSON.parse(localStorage.getItem("list"));
console.log(savedlist)

const input = document.getElementById("button");

input.addEventListener("click", function (e) {
  e.preventDefault();
  var title = document.getElementById("name").value;
  var description = document.getElementById("description").value;
  var dueDate = "soon";
  var priority = 0;
  var project = todoList.currentProject;
  todoList.add(title, description, dueDate, priority, project);
});
