import { todoList } from "./todolist.js";
import { render } from "./render.js";


export function nav() {
  const nav = document.getElementById("nav");
  nav.innerHTML = "";
  const ls = todoList.showAll();

  for (var i = 0; i < ls.length; i++) {
    const nav = document.getElementById("nav");
    let button = document.createElement("button");
    button.innerHTML = ls[i].name;
    button.classList.add("navButton");
    button.setAttribute("id", i);
    nav.append(button);
  };

  let inputField = document.createElement("input");
  inputField.setAttribute("id", "newProjectName");
  let button = document.createElement("button");
    button.innerHTML = "Add Project";
    button.classList.add("addProject");
    button.setAttribute("id", "addProject");
    nav.append(inputField);
    nav.append(button);
    button.addEventListener("click", () => todoList.addProject(newProjectName.value));

  var navButton = document.getElementsByClassName("navButton");

  Array.from(navButton).forEach(function (element) {
    element.addEventListener("click", () => render(element.id));
  });


}
