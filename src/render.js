import { todoList } from "./todolist.js";

export function render(id) {
  todoList.currentProject = id;
  const content = document.getElementById("content");
  content.innerHTML = "";
  var ls = todoList.show(id).todos;

 let projectTitle = document.createElement("h1");
 projectTitle.innerHTML = todoList.show(id).name;

 let removeProject = document.createElement("button");
 removeProject.innerHTML = "Delete";
 removeProject.addEventListener("click", () => todoList.removeProject(id));

 content.append(projectTitle);
 content.append(removeProject);

 let todos = document.createElement("div");
content.append(todos)

  for (var i = 0; i < ls.length; i++) {
    let box = document.createElement("div");
    box.classList.add("todoBox");

    const content = document.getElementById("content");
    let h1 = document.createElement("h1");
    let p = document.createElement("p");
    let button = document.createElement("button");
    h1.innerHTML = ls[i].title;
    p.innerHTML = ls[i].description;
    button.innerHTML = "close";
    button.classList.add("closeButton");
    button.setAttribute("id", i);
    box.append(h1);
    box.append(p);
    box.append(button);
    todos.append(box);

  }

  var closeButtons = document.getElementsByClassName("closeButton");

  Array.from(closeButtons).forEach(function (element) {
    element.addEventListener("click", () => todoList.remove(element, id));
  });
}
