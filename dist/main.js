/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/nav.js":
/*!********************!*\
  !*** ./src/nav.js ***!
  \********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   nav: () => (/* binding */ nav)
/* harmony export */ });
/* harmony import */ var _todolist_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./todolist.js */ "./src/todolist.js");
/* harmony import */ var _render_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./render.js */ "./src/render.js");




function nav() {
  const nav = document.getElementById("nav");
  nav.innerHTML = "";
  const ls = _todolist_js__WEBPACK_IMPORTED_MODULE_0__.todoList.showAll();

  for (var i = 0; i < ls.length; i++) {
    const nav = document.getElementById("nav");
    let button = document.createElement("button");
    button.innerHTML = ls[i].name;
    button.classList.add("navButton");
    button.setAttribute("id", i);
    nav.append(button);
  };

  let inputField = document.createElement("input");
  inputField.setAttribute("id", "newProjectName");
  let button = document.createElement("button");
    button.innerHTML = "Add Project";
    button.classList.add("addProject");
    button.setAttribute("id", "addProject");
    nav.append(inputField);
    nav.append(button);
    button.addEventListener("click", () => _todolist_js__WEBPACK_IMPORTED_MODULE_0__.todoList.addProject(newProjectName.value));

  var navButton = document.getElementsByClassName("navButton");

  Array.from(navButton).forEach(function (element) {
    element.addEventListener("click", () => (0,_render_js__WEBPACK_IMPORTED_MODULE_1__.render)(element.id));
  });


}


/***/ }),

/***/ "./src/render.js":
/*!***********************!*\
  !*** ./src/render.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: () => (/* binding */ render)
/* harmony export */ });
/* harmony import */ var _todolist_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./todolist.js */ "./src/todolist.js");


function render(id) {
  _todolist_js__WEBPACK_IMPORTED_MODULE_0__.todoList.currentProject = id;
  const content = document.getElementById("content");
  content.innerHTML = "";
  var ls = _todolist_js__WEBPACK_IMPORTED_MODULE_0__.todoList.show(id).todos;

 let projectTitle = document.createElement("h1");
 projectTitle.innerHTML = _todolist_js__WEBPACK_IMPORTED_MODULE_0__.todoList.show(id).name;

 let removeProject = document.createElement("button");
 removeProject.innerHTML = "Delete";
 removeProject.addEventListener("click", () => _todolist_js__WEBPACK_IMPORTED_MODULE_0__.todoList.removeProject(id));

 content.append(projectTitle);
 content.append(removeProject);

 let todos = document.createElement("div");
content.append(todos)

  for (var i = 0; i < ls.length; i++) {
    let box = document.createElement("div");
    box.classList.add("todoBox");

    const content = document.getElementById("content");
    let h1 = document.createElement("h1");
    let p = document.createElement("p");
    let button = document.createElement("button");
    h1.innerHTML = ls[i].title;
    p.innerHTML = ls[i].description;
    button.innerHTML = "close";
    button.classList.add("closeButton");
    button.setAttribute("id", i);
    box.append(h1);
    box.append(p);
    box.append(button);
    todos.append(box);

  }

  var closeButtons = document.getElementsByClassName("closeButton");

  Array.from(closeButtons).forEach(function (element) {
    element.addEventListener("click", () => _todolist_js__WEBPACK_IMPORTED_MODULE_0__.todoList.remove(element, id));
  });
}


/***/ }),

/***/ "./src/todolist.js":
/*!*************************!*\
  !*** ./src/todolist.js ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   todoList: () => (/* binding */ todoList)
/* harmony export */ });
/* harmony import */ var _render_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./render.js */ "./src/render.js");
/* harmony import */ var _nav_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./nav.js */ "./src/nav.js");




var todoList = (() => {
  var currentProject = 0;

  var todo = [
    {
      name: "Default",
      todos: [
        {
          title: "Default",
          description: "test",
          dueDate: "test",
          priority: 0,
        },
        {
          title: "test2",
          description: "test2",
          dueDate: "test2",
          priority: 0,
        },
      ],
    },

    {
      name: "Project2",
      todos: [
        {
          title: "Project2",
          description: "test",
          dueDate: "test",
          priority: 0,
        },
        {
          title: "Project2",
          description: "2",
          dueDate: "test",
          priority: 0,
        },
      ],
    },
  ];

  function show(e) {
    return todo[e];
  }

  function showAll() {
    return todo;
  }

  function add(title, description, dueDate, priority, project) {
    todo[project].todos.push({
      title: title,
      description: description,
      dueDate: dueDate,
      priority: priority,
    });
    save();
    (0,_render_js__WEBPACK_IMPORTED_MODULE_0__.render)(project);
  }

  function remove(element, arrayId) {
    console.log(todo[arrayId].todos[element.id]);
    todo[arrayId].todos.splice(element.id, 1);
    save();
    (0,_render_js__WEBPACK_IMPORTED_MODULE_0__.render)(arrayId);
  }

  function addProject(newName) {
    console.log("add project")
    todo.push({name: newName, todos: []})
    ;(0,_nav_js__WEBPACK_IMPORTED_MODULE_1__.nav)();
    save();
  }

  function removeProject(id) {
    todo.splice(id,1)
    save();
    (0,_nav_js__WEBPACK_IMPORTED_MODULE_1__.nav)()
    ;(0,_render_js__WEBPACK_IMPORTED_MODULE_0__.render)(0);
  }

  function save() {
    localStorage.setItem("list", JSON.stringify(todo));
    console.log("saved");
  }

  function load() {
    const savedlist = JSON.parse(localStorage.getItem("list"));
    if (savedlist) {
      todo = savedlist;
    } else {
      alert("no data!");
    }
  }

  return {
    show: show,
    showAll: showAll,
    add: add,
    remove: remove,
    load: load,
    addProject: addProject,
    removeProject: removeProject,
    currentProject: currentProject,
  };
})();


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _render_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./render.js */ "./src/render.js");
/* harmony import */ var _todolist_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./todolist.js */ "./src/todolist.js");
/* harmony import */ var _nav_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./nav.js */ "./src/nav.js");




_todolist_js__WEBPACK_IMPORTED_MODULE_1__.todoList.load();
(0,_nav_js__WEBPACK_IMPORTED_MODULE_2__.nav)();
(0,_render_js__WEBPACK_IMPORTED_MODULE_0__.render)(0);

const savedlist = JSON.parse(localStorage.getItem("list"));
console.log(savedlist)

const input = document.getElementById("button");

input.addEventListener("click", function (e) {
  e.preventDefault();
  var title = document.getElementById("name").value;
  var description = document.getElementById("description").value;
  var dueDate = "soon";
  var priority = 0;
  var project = _todolist_js__WEBPACK_IMPORTED_MODULE_1__.todoList.currentProject;
  _todolist_js__WEBPACK_IMPORTED_MODULE_1__.todoList.add(title, description, dueDate, priority, project);
});

})();

/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQXlDO0FBQ0o7OztBQUc5QjtBQUNQO0FBQ0E7QUFDQSxhQUFhLGtEQUFROztBQUVyQixrQkFBa0IsZUFBZTtBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkNBQTJDLGtEQUFROztBQUVuRDs7QUFFQTtBQUNBLDRDQUE0QyxrREFBTTtBQUNsRCxHQUFHOzs7QUFHSDs7Ozs7Ozs7Ozs7Ozs7OztBQ25DeUM7O0FBRWxDO0FBQ1AsRUFBRSxrREFBUTtBQUNWO0FBQ0E7QUFDQSxXQUFXLGtEQUFROztBQUVuQjtBQUNBLDBCQUEwQixrREFBUTs7QUFFbEM7QUFDQTtBQUNBLCtDQUErQyxrREFBUTs7QUFFdkQ7QUFDQTs7QUFFQTtBQUNBOztBQUVBLGtCQUFrQixlQUFlO0FBQ2pDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQSw0Q0FBNEMsa0RBQVE7QUFDcEQsR0FBRztBQUNIOzs7Ozs7Ozs7Ozs7Ozs7OztBQzlDcUM7QUFDTjs7O0FBR3hCO0FBQ1A7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLElBQUksa0RBQU07QUFDVjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksa0RBQU07QUFDVjs7QUFFQTtBQUNBO0FBQ0EsZUFBZSx5QkFBeUI7QUFDeEMsSUFBSSw2Q0FBRztBQUNQO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsSUFBSSw0Q0FBRztBQUNQLElBQUksbURBQU07QUFDVjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOzs7Ozs7O1VDN0dEO1VBQ0E7O1VBRUE7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7O1VBRUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7Ozs7O1dDdEJBO1dBQ0E7V0FDQTtXQUNBO1dBQ0EseUNBQXlDLHdDQUF3QztXQUNqRjtXQUNBO1dBQ0E7Ozs7O1dDUEE7Ozs7O1dDQUE7V0FDQTtXQUNBO1dBQ0EsdURBQXVELGlCQUFpQjtXQUN4RTtXQUNBLGdEQUFnRCxhQUFhO1dBQzdEOzs7Ozs7Ozs7Ozs7OztBQ05xQztBQUNJO0FBQ1Y7O0FBRS9CLGtEQUFRO0FBQ1IsNENBQUc7QUFDSCxrREFBTTs7QUFFTjtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQixrREFBUTtBQUN4QixFQUFFLGtEQUFRO0FBQ1YsQ0FBQyIsInNvdXJjZXMiOlsid2VicGFjazovL29kaW4tdG9kby8uL3NyYy9uYXYuanMiLCJ3ZWJwYWNrOi8vb2Rpbi10b2RvLy4vc3JjL3JlbmRlci5qcyIsIndlYnBhY2s6Ly9vZGluLXRvZG8vLi9zcmMvdG9kb2xpc3QuanMiLCJ3ZWJwYWNrOi8vb2Rpbi10b2RvL3dlYnBhY2svYm9vdHN0cmFwIiwid2VicGFjazovL29kaW4tdG9kby93ZWJwYWNrL3J1bnRpbWUvZGVmaW5lIHByb3BlcnR5IGdldHRlcnMiLCJ3ZWJwYWNrOi8vb2Rpbi10b2RvL3dlYnBhY2svcnVudGltZS9oYXNPd25Qcm9wZXJ0eSBzaG9ydGhhbmQiLCJ3ZWJwYWNrOi8vb2Rpbi10b2RvL3dlYnBhY2svcnVudGltZS9tYWtlIG5hbWVzcGFjZSBvYmplY3QiLCJ3ZWJwYWNrOi8vb2Rpbi10b2RvLy4vc3JjL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IHRvZG9MaXN0IH0gZnJvbSBcIi4vdG9kb2xpc3QuanNcIjtcbmltcG9ydCB7IHJlbmRlciB9IGZyb20gXCIuL3JlbmRlci5qc1wiO1xuXG5cbmV4cG9ydCBmdW5jdGlvbiBuYXYoKSB7XG4gIGNvbnN0IG5hdiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibmF2XCIpO1xuICBuYXYuaW5uZXJIVE1MID0gXCJcIjtcbiAgY29uc3QgbHMgPSB0b2RvTGlzdC5zaG93QWxsKCk7XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBscy5sZW5ndGg7IGkrKykge1xuICAgIGNvbnN0IG5hdiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibmF2XCIpO1xuICAgIGxldCBidXR0b24gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiYnV0dG9uXCIpO1xuICAgIGJ1dHRvbi5pbm5lckhUTUwgPSBsc1tpXS5uYW1lO1xuICAgIGJ1dHRvbi5jbGFzc0xpc3QuYWRkKFwibmF2QnV0dG9uXCIpO1xuICAgIGJ1dHRvbi5zZXRBdHRyaWJ1dGUoXCJpZFwiLCBpKTtcbiAgICBuYXYuYXBwZW5kKGJ1dHRvbik7XG4gIH07XG5cbiAgbGV0IGlucHV0RmllbGQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiaW5wdXRcIik7XG4gIGlucHV0RmllbGQuc2V0QXR0cmlidXRlKFwiaWRcIiwgXCJuZXdQcm9qZWN0TmFtZVwiKTtcbiAgbGV0IGJ1dHRvbiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJidXR0b25cIik7XG4gICAgYnV0dG9uLmlubmVySFRNTCA9IFwiQWRkIFByb2plY3RcIjtcbiAgICBidXR0b24uY2xhc3NMaXN0LmFkZChcImFkZFByb2plY3RcIik7XG4gICAgYnV0dG9uLnNldEF0dHJpYnV0ZShcImlkXCIsIFwiYWRkUHJvamVjdFwiKTtcbiAgICBuYXYuYXBwZW5kKGlucHV0RmllbGQpO1xuICAgIG5hdi5hcHBlbmQoYnV0dG9uKTtcbiAgICBidXR0b24uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsICgpID0+IHRvZG9MaXN0LmFkZFByb2plY3QobmV3UHJvamVjdE5hbWUudmFsdWUpKTtcblxuICB2YXIgbmF2QnV0dG9uID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcIm5hdkJ1dHRvblwiKTtcblxuICBBcnJheS5mcm9tKG5hdkJ1dHRvbikuZm9yRWFjaChmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgIGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsICgpID0+IHJlbmRlcihlbGVtZW50LmlkKSk7XG4gIH0pO1xuXG5cbn1cbiIsImltcG9ydCB7IHRvZG9MaXN0IH0gZnJvbSBcIi4vdG9kb2xpc3QuanNcIjtcblxuZXhwb3J0IGZ1bmN0aW9uIHJlbmRlcihpZCkge1xuICB0b2RvTGlzdC5jdXJyZW50UHJvamVjdCA9IGlkO1xuICBjb25zdCBjb250ZW50ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjb250ZW50XCIpO1xuICBjb250ZW50LmlubmVySFRNTCA9IFwiXCI7XG4gIHZhciBscyA9IHRvZG9MaXN0LnNob3coaWQpLnRvZG9zO1xuXG4gbGV0IHByb2plY3RUaXRsZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJoMVwiKTtcbiBwcm9qZWN0VGl0bGUuaW5uZXJIVE1MID0gdG9kb0xpc3Quc2hvdyhpZCkubmFtZTtcblxuIGxldCByZW1vdmVQcm9qZWN0ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImJ1dHRvblwiKTtcbiByZW1vdmVQcm9qZWN0LmlubmVySFRNTCA9IFwiRGVsZXRlXCI7XG4gcmVtb3ZlUHJvamVjdC5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgKCkgPT4gdG9kb0xpc3QucmVtb3ZlUHJvamVjdChpZCkpO1xuXG4gY29udGVudC5hcHBlbmQocHJvamVjdFRpdGxlKTtcbiBjb250ZW50LmFwcGVuZChyZW1vdmVQcm9qZWN0KTtcblxuIGxldCB0b2RvcyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XG5jb250ZW50LmFwcGVuZCh0b2RvcylcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IGxzLmxlbmd0aDsgaSsrKSB7XG4gICAgbGV0IGJveCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XG4gICAgYm94LmNsYXNzTGlzdC5hZGQoXCJ0b2RvQm94XCIpO1xuXG4gICAgY29uc3QgY29udGVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY29udGVudFwiKTtcbiAgICBsZXQgaDEgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiaDFcIik7XG4gICAgbGV0IHAgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwicFwiKTtcbiAgICBsZXQgYnV0dG9uID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImJ1dHRvblwiKTtcbiAgICBoMS5pbm5lckhUTUwgPSBsc1tpXS50aXRsZTtcbiAgICBwLmlubmVySFRNTCA9IGxzW2ldLmRlc2NyaXB0aW9uO1xuICAgIGJ1dHRvbi5pbm5lckhUTUwgPSBcImNsb3NlXCI7XG4gICAgYnV0dG9uLmNsYXNzTGlzdC5hZGQoXCJjbG9zZUJ1dHRvblwiKTtcbiAgICBidXR0b24uc2V0QXR0cmlidXRlKFwiaWRcIiwgaSk7XG4gICAgYm94LmFwcGVuZChoMSk7XG4gICAgYm94LmFwcGVuZChwKTtcbiAgICBib3guYXBwZW5kKGJ1dHRvbik7XG4gICAgdG9kb3MuYXBwZW5kKGJveCk7XG5cbiAgfVxuXG4gIHZhciBjbG9zZUJ1dHRvbnMgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKFwiY2xvc2VCdXR0b25cIik7XG5cbiAgQXJyYXkuZnJvbShjbG9zZUJ1dHRvbnMpLmZvckVhY2goZnVuY3Rpb24gKGVsZW1lbnQpIHtcbiAgICBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCAoKSA9PiB0b2RvTGlzdC5yZW1vdmUoZWxlbWVudCwgaWQpKTtcbiAgfSk7XG59XG4iLCJpbXBvcnQgeyByZW5kZXIgfSBmcm9tIFwiLi9yZW5kZXIuanNcIjtcbmltcG9ydCB7IG5hdiB9IGZyb20gXCIuL25hdi5qc1wiO1xuXG5cbmV4cG9ydCB2YXIgdG9kb0xpc3QgPSAoKCkgPT4ge1xuICB2YXIgY3VycmVudFByb2plY3QgPSAwO1xuXG4gIHZhciB0b2RvID0gW1xuICAgIHtcbiAgICAgIG5hbWU6IFwiRGVmYXVsdFwiLFxuICAgICAgdG9kb3M6IFtcbiAgICAgICAge1xuICAgICAgICAgIHRpdGxlOiBcIkRlZmF1bHRcIixcbiAgICAgICAgICBkZXNjcmlwdGlvbjogXCJ0ZXN0XCIsXG4gICAgICAgICAgZHVlRGF0ZTogXCJ0ZXN0XCIsXG4gICAgICAgICAgcHJpb3JpdHk6IDAsXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICB0aXRsZTogXCJ0ZXN0MlwiLFxuICAgICAgICAgIGRlc2NyaXB0aW9uOiBcInRlc3QyXCIsXG4gICAgICAgICAgZHVlRGF0ZTogXCJ0ZXN0MlwiLFxuICAgICAgICAgIHByaW9yaXR5OiAwLFxuICAgICAgICB9LFxuICAgICAgXSxcbiAgICB9LFxuXG4gICAge1xuICAgICAgbmFtZTogXCJQcm9qZWN0MlwiLFxuICAgICAgdG9kb3M6IFtcbiAgICAgICAge1xuICAgICAgICAgIHRpdGxlOiBcIlByb2plY3QyXCIsXG4gICAgICAgICAgZGVzY3JpcHRpb246IFwidGVzdFwiLFxuICAgICAgICAgIGR1ZURhdGU6IFwidGVzdFwiLFxuICAgICAgICAgIHByaW9yaXR5OiAwLFxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgdGl0bGU6IFwiUHJvamVjdDJcIixcbiAgICAgICAgICBkZXNjcmlwdGlvbjogXCIyXCIsXG4gICAgICAgICAgZHVlRGF0ZTogXCJ0ZXN0XCIsXG4gICAgICAgICAgcHJpb3JpdHk6IDAsXG4gICAgICAgIH0sXG4gICAgICBdLFxuICAgIH0sXG4gIF07XG5cbiAgZnVuY3Rpb24gc2hvdyhlKSB7XG4gICAgcmV0dXJuIHRvZG9bZV07XG4gIH1cblxuICBmdW5jdGlvbiBzaG93QWxsKCkge1xuICAgIHJldHVybiB0b2RvO1xuICB9XG5cbiAgZnVuY3Rpb24gYWRkKHRpdGxlLCBkZXNjcmlwdGlvbiwgZHVlRGF0ZSwgcHJpb3JpdHksIHByb2plY3QpIHtcbiAgICB0b2RvW3Byb2plY3RdLnRvZG9zLnB1c2goe1xuICAgICAgdGl0bGU6IHRpdGxlLFxuICAgICAgZGVzY3JpcHRpb246IGRlc2NyaXB0aW9uLFxuICAgICAgZHVlRGF0ZTogZHVlRGF0ZSxcbiAgICAgIHByaW9yaXR5OiBwcmlvcml0eSxcbiAgICB9KTtcbiAgICBzYXZlKCk7XG4gICAgcmVuZGVyKHByb2plY3QpO1xuICB9XG5cbiAgZnVuY3Rpb24gcmVtb3ZlKGVsZW1lbnQsIGFycmF5SWQpIHtcbiAgICBjb25zb2xlLmxvZyh0b2RvW2FycmF5SWRdLnRvZG9zW2VsZW1lbnQuaWRdKTtcbiAgICB0b2RvW2FycmF5SWRdLnRvZG9zLnNwbGljZShlbGVtZW50LmlkLCAxKTtcbiAgICBzYXZlKCk7XG4gICAgcmVuZGVyKGFycmF5SWQpO1xuICB9XG5cbiAgZnVuY3Rpb24gYWRkUHJvamVjdChuZXdOYW1lKSB7XG4gICAgY29uc29sZS5sb2coXCJhZGQgcHJvamVjdFwiKVxuICAgIHRvZG8ucHVzaCh7bmFtZTogbmV3TmFtZSwgdG9kb3M6IFtdfSlcbiAgICBuYXYoKTtcbiAgICBzYXZlKCk7XG4gIH1cblxuICBmdW5jdGlvbiByZW1vdmVQcm9qZWN0KGlkKSB7XG4gICAgdG9kby5zcGxpY2UoaWQsMSlcbiAgICBzYXZlKCk7XG4gICAgbmF2KClcbiAgICByZW5kZXIoMCk7XG4gIH1cblxuICBmdW5jdGlvbiBzYXZlKCkge1xuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwibGlzdFwiLCBKU09OLnN0cmluZ2lmeSh0b2RvKSk7XG4gICAgY29uc29sZS5sb2coXCJzYXZlZFwiKTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGxvYWQoKSB7XG4gICAgY29uc3Qgc2F2ZWRsaXN0ID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImxpc3RcIikpO1xuICAgIGlmIChzYXZlZGxpc3QpIHtcbiAgICAgIHRvZG8gPSBzYXZlZGxpc3Q7XG4gICAgfSBlbHNlIHtcbiAgICAgIGFsZXJ0KFwibm8gZGF0YSFcIik7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBzaG93OiBzaG93LFxuICAgIHNob3dBbGw6IHNob3dBbGwsXG4gICAgYWRkOiBhZGQsXG4gICAgcmVtb3ZlOiByZW1vdmUsXG4gICAgbG9hZDogbG9hZCxcbiAgICBhZGRQcm9qZWN0OiBhZGRQcm9qZWN0LFxuICAgIHJlbW92ZVByb2plY3Q6IHJlbW92ZVByb2plY3QsXG4gICAgY3VycmVudFByb2plY3Q6IGN1cnJlbnRQcm9qZWN0LFxuICB9O1xufSkoKTtcbiIsIi8vIFRoZSBtb2R1bGUgY2FjaGVcbnZhciBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX18gPSB7fTtcblxuLy8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbmZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG5cdHZhciBjYWNoZWRNb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdO1xuXHRpZiAoY2FjaGVkTW9kdWxlICE9PSB1bmRlZmluZWQpIHtcblx0XHRyZXR1cm4gY2FjaGVkTW9kdWxlLmV4cG9ydHM7XG5cdH1cblx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcblx0dmFyIG1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF0gPSB7XG5cdFx0Ly8gbm8gbW9kdWxlLmlkIG5lZWRlZFxuXHRcdC8vIG5vIG1vZHVsZS5sb2FkZWQgbmVlZGVkXG5cdFx0ZXhwb3J0czoge31cblx0fTtcblxuXHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cblx0X193ZWJwYWNrX21vZHVsZXNfX1ttb2R1bGVJZF0obW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cblx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcblx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xufVxuXG4iLCIvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9ucyBmb3IgaGFybW9ueSBleHBvcnRzXG5fX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSAoZXhwb3J0cywgZGVmaW5pdGlvbikgPT4ge1xuXHRmb3IodmFyIGtleSBpbiBkZWZpbml0aW9uKSB7XG5cdFx0aWYoX193ZWJwYWNrX3JlcXVpcmVfXy5vKGRlZmluaXRpb24sIGtleSkgJiYgIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBrZXkpKSB7XG5cdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywga2V5LCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZGVmaW5pdGlvbltrZXldIH0pO1xuXHRcdH1cblx0fVxufTsiLCJfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSAob2JqLCBwcm9wKSA9PiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iaiwgcHJvcCkpIiwiLy8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuX193ZWJwYWNrX3JlcXVpcmVfXy5yID0gKGV4cG9ydHMpID0+IHtcblx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG5cdH1cblx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbn07IiwiaW1wb3J0IHsgcmVuZGVyIH0gZnJvbSBcIi4vcmVuZGVyLmpzXCI7XG5pbXBvcnQgeyB0b2RvTGlzdCB9IGZyb20gXCIuL3RvZG9saXN0LmpzXCI7XG5pbXBvcnQgeyBuYXYgfSBmcm9tIFwiLi9uYXYuanNcIjtcblxudG9kb0xpc3QubG9hZCgpO1xubmF2KCk7XG5yZW5kZXIoMCk7XG5cbmNvbnN0IHNhdmVkbGlzdCA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJsaXN0XCIpKTtcbmNvbnNvbGUubG9nKHNhdmVkbGlzdClcblxuY29uc3QgaW5wdXQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImJ1dHRvblwiKTtcblxuaW5wdXQuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uIChlKSB7XG4gIGUucHJldmVudERlZmF1bHQoKTtcbiAgdmFyIHRpdGxlID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJuYW1lXCIpLnZhbHVlO1xuICB2YXIgZGVzY3JpcHRpb24gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImRlc2NyaXB0aW9uXCIpLnZhbHVlO1xuICB2YXIgZHVlRGF0ZSA9IFwic29vblwiO1xuICB2YXIgcHJpb3JpdHkgPSAwO1xuICB2YXIgcHJvamVjdCA9IHRvZG9MaXN0LmN1cnJlbnRQcm9qZWN0O1xuICB0b2RvTGlzdC5hZGQodGl0bGUsIGRlc2NyaXB0aW9uLCBkdWVEYXRlLCBwcmlvcml0eSwgcHJvamVjdCk7XG59KTtcbiJdLCJuYW1lcyI6W10sInNvdXJjZVJvb3QiOiIifQ==